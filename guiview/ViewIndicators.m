classdef ViewIndicators < ViewBase
    properties
        controlObj
        tablayout
    end
    
    methods
        function obj = ViewIndicators()
            obj = obj@ViewBase('ViewIndicators');
            obj.viewSize  =    [20,50,800,500];
            obj.buildUI();
            obj.makeController();
            obj.attachToController();
            obj.registerSelf();
            set(obj.hfig,'Visible','on');
        end
    end
    
    
    methods
        function buildUI(obj)
            set(obj.hfig,'Toolbar','figure');
            obj.buildMainWindow();
            set(obj.hfig,'Visible','off');
            
            %%%%
            % layout includes all the tabs
            %%%%
            viewLayout   = uix.VBox('Parent',obj.hfig);  % over all layout
            obj.tablayout  = uix.TabPanel('Parent',viewLayout);
            
            
            %%%%%%%%%%%%%%%%%%%
            % tab 1
            %%%%%%%%%%%%%%%%%%%
            %Tab1View = uiextras.VBox('Parent',tablayout);
            %panelcurrent= uipanel('Parent',Tab1View,'Title','Test1','FontSize',8);
            
            
            
            
            
        end
        
        function buildMainWindow(obj)
            obj.hfig =  figure('pos',obj.viewSize,'tag','Configuration');
            %             set(obj.hfig,'resize','off');
            %             set(obj.hfig,'menubar','none');
            set(obj.hfig,'numbertitle','off');
        end
        
        
        function controlObj = makeController(obj)
            controlObj = ControllerIndicators.getController();
            obj.controlObj = controlObj ;
        end
        
        
        
        function attachToController(obj)
            controller = obj.controlObj;
            
            
            updateplot = @obj.updateplot;
            mvcManager =  MVCManager.getMVCManager();
            modelObj = mvcManager.getData('ModelIndicators');
            modelObj.addlistener('updateplot',updateplot);
            
        end
        
        function updateplot(obj, a,b)
            mvcManager =  MVCManager.getMVCManager();
            modelobj = mvcManager.getData('ModelIndicators');
            stock = modelobj.stockdata;
            
            keys = stock.indicators.keys;
            tabNames={};
            for iter = 1 : length(keys)
                % only the indicators knows how to plot itself
                % so dispatching to the indicators
                indicator = stock.indicators(keys{iter});
                tab  = uiextras.VBox('Parent',obj.tablayout);
                panel= uipanel('Parent',tab,'Title',indicator.name,'FontSize',8);
                tabNames{iter} = indicator.name;
                hold on;
                indicator.plotOnPanel(panel);
            end
            
            
            % extra tab to merge all two
            tab  = uiextras.VBox('Parent',obj.tablayout);
            panel_new= uipanel('Parent',tab,'Title','MA & MACD','FontSize',8);
            tabNames{3} = 'MA & MACD';
            
            axesobj  = axes('Parent', panel_new);
            ind_1 =  stock.indicators('MACD');
            ind_1.plotOnItem(axesobj);
            ind_2=stock.indicators('MA5_20');
            ind_2.plotOnItem(axesobj);
            
            obj.tablayout.TabTitles = tabNames;
            set(obj.hfig,'Visible','on');
        end
        
    end
    
end
