classdef ViewBase < handle
    properties
        viewSize      
        hfig          
        ID            

    end
    
    methods
        function obj = ViewBase(ID)
            obj.ID = ID;
        end
        function registerSelf(obj)
           mvcManagerObj =  MVCManager.getMVCManager();
           mvcManagerObj.register(obj,obj.ID);
        end
    end

    
    methods(Abstract)
        controlObj = makeController(obj)
        buildUI(obj);
    end
    
end

