classdef Writer < handle 
    methods(Abstract)
        write(obj,filename);
    end
end