classdef DateRange < handle
    properties
        startdate   % integer
        enddate
    end
    methods
        function obj = DateRange(startday, endday)
            if nargin ~= 2
                ME = MException('DateRange:InputNum',...
                    'Input num incorrect');
                throw(ME);
            end
            if ischar(startday) && ischar(endday)
                obj.startdate = datenum(startday);   %
                if strcmp(endday,'today') || strcmp(endday,'Today')
                    obj.enddate = datenum(today);
                else
                    obj.enddate = datenum(endday);
                end
            elseif isnumeric(startday) && isnumeric(endday) && ...
                    startday> 0 && endday> 0  && ...
                    endday > startday
                obj.startdate = startday;
                obj.enddate = endday;
            elseif isnumeric(startday) && isnumeric(endday)&&...
                    startday ==0 && endday==0
                % internal null state, need this one for minus
                obj.startdate =0;
                obj.enddate=0;
            elseif isinf(startday) && isnumeric(endday) && ...
                    endday>0;
                obj.startdate=inf;
                obj.enddate=endday;
            else
                ME = MException('DateRange:InvalidInput',...
                    'Invalid Input');
                throw(ME);
            end
        end
        function newobj = minus(o1,o2) % o1-o2
            % o1 needs to be a subset of o2 otherwise throw exception
            if o2 == DateRange(0,0)
               newobj = o1;
            elseif o1.startdate ==o2.startdate && o1.enddate ==o2.enddate    
               % same day minus, equal to no operation
               newobj = DateRange(inf,inf);
            elseif o1.startdate < o2.startdate 
                % recurse call minus
                newobj = DateRange(o2.startdate,o1.enddate) - o2;
            elseif o1.startdate > o2.startdate  || ...
                    o1.enddate < o2.enddate 
                ME = MException('DateRange:MinusError',...
                    'Invalid Minus');
                throw(ME);
            elseif o1.startdate == o2.startdate  && ...
                    o1.enddate == o2.enddate
                ME = MException('DateRange:MinusError',...
                    'Invalid Minus');
                throw(ME);
            elseif  o1.startdate < o2.startdate && ...
                    o1.enddate > o2.enddate
                newobj = DateRange(o2.startdate,o1.enddate) ;
            elseif o1.startdate == o2.startdate && ...
                    o1.enddate > o2.enddate
                newobj = DateRange(o2.enddate,o1.enddate);
            end
            
            % regulize 
           if o2.enddate == (o1.enddate-1)
                   newobj.startdate = inf; 
           end
        end
        function boolv = eq(o1,o2)
            boolv = (o1.startdate == o2.startdate) && ...
                (o1.enddate == o2.enddate) ;
        end
        
        function boolv = neq(o1,o2)
            boolv = (o1.startdate ~= o2.startdate) || ...
                (o1.enddate ~= o2.enddate) ;
        end
    end
end
