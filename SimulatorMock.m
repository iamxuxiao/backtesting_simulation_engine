classdef SimulatorMock < Simulator
    properties
        %> This is a universal clock
        clock
        stock
        %> engine will own the account
        engine
        %strategy
        strategymixer
        log
    end
    events 
        simulation_end
    end
    events
        instructions_generate_signal
    end
    methods
        
        function obj = SimulatorMock()
          
        end
        function startSimulation(obj)
           
        end
        
        function eventBlock(obj) 
           
        end
    end
end