function runbacktestsuite(varargin)


p = inputParser;
p.addRequired('testToRun',  @(x)isa(x,'handle'));
p.addParamValue('harness', BackTestingHarness, @(x)isa(x,'BackTestingHarness'));
p.parse(varargin{:});

inputs = p.Results;
harness = inputs.harness;
testToRun = inputs.testToRun;

fh_testsetup=str2func('testSetup');
fh_testTearDown=str2func('testTearDown'); 
fh_testInit=str2func('testInit');  % read in config , feed to harness 
fh_whichDataToSimulate=str2func('whichDataToSimulate');
fh_testStrategyOnStock=str2func('testStrategyOnStock');

fh_testsetup(testToRun);
fh_testTearDown(testToRun);
fh_testInit(testToRun);

harness.setup_path();
harness.setup_testToRun(testToRun);
harness.setup_datasource();

pathes=fh_whichDataToSimulate(testToRun);
% loop all the stocks 
for iter = 1: length(pathes)
    
    a_stock = StockSimpleData(pathes{iter});
    sim =  SimulatorBackTesting(testToRun.timerangeobj,a_stock);
    account = AccountSimple(testToRun.initialAmout,sim,a_stock.sym);  % user
    sim.engine = ExecutionEngine(account, sim.strategymixer);  % developer
    fh_testStrategyOnStock(testToRun,a_stock,account,sim);
    
end

% drive the simulation 
%fh_testTraverseAllStocks(testToRun);