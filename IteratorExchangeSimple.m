classdef IteratorExchangeSimple < handle 
    properties
        aggHandle
        counter
    end
    methods
        function obj =  IteratorExchangeSimple(agg)
            obj.aggHandle = agg ; 
            obj.counter =1 ;
        end
        function symname = first(obj)
           symname = obj.aggHandle.symbols{1}
        end
        function symname = currentItem(obj)
            symname = obj.aggHandle.symbols{obj.counter};
        end
        function nextItem(obj)
            obj.counter = obj.counter + 1;
        end
        function boolval = isDone(obj)
           boolval = (obj.counter == obj.aggHandle.totalnum+1); 
        end
            
    end
end