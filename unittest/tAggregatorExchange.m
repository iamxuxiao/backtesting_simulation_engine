classdef tAggregatorExchange < matlab.unittest.TestCase
    methods(Test)
        function testReadingCSVright(testCase)

            o = AggregatorExchange('.\resources\symbols\unittestData_nasdaq_5.csv');
            testCase.verifyTrue(o.totalnum==5,'');
            testCase.verifyNotEmpty(o.iterator);
            
        end
    end
end