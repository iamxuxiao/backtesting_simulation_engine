classdef tdatafeed < matlab.unittest.TestCase
 
    properties
        TestFigure
    end
 
    methods(TestMethodSetup)
        function createFigure(testCase)
              if exist('.\derived\datafeed','dir')~=7
                mkdir('.\derived\datafeed');
            else
                rmdir('.\derived\datafeed','s');
            end
        end
    end
 
    methods(TestMethodTeardown)
 function teardir(testCase)
            pause(7);
            if exist('.\derived\datafeed','dir')==7
                rmdir('.\derived\datafeed','s');
            end
        end
    end
 
    methods(Test)
 
        function defaultCurrentPoint(testCase)
             config = ReaderConfig('.\config\nasdaq_unittest.xml');
            directorfeed = DirectorDatafeed;
            simplebuilder = BuilderSimple();
            simplebuilder.inputdaterange = DateRange(config.data.startdate,config.data.enddate);
            directorfeed.builder = simplebuilder;
            directorfeed.work();
        end
 
     
 
    end
 
end