classdef tclock < matlab.unittest.TestCase
    methods(Test)
        function testInvalidInput(testCase)
            % if ran   first , will throw
            rangeObj = DateRange('1/1/2015','4/1/2015');
            o = ClockSimple.getInstance(rangeObj);
            o.delete();
            testCase.verifyError(@()ClockSimple.getInstance(),'Clock:init','')
        end
        
        
        
        function testInput(testCase)
            rangeObj = DateRange('1/1/2015','4/1/2015');
            o = ClockSimple.getInstance(rangeObj);
            
            % independent varify
            t1 = datenum('1/1/2015');
            t2 = datenum('4/1/2015');
            tarray = [t1:1:t2];
            tlogical  = isbusday( tarray);
            tExp = tarray(tlogical);
            
            testCase.verifyEqual(o.busdays,tExp,'');
        end
        
        
        function testWiping(testCase)
            rangeObj = DateRange('1/1/2015','4/1/2015');
            o = ClockSimple.getInstance(rangeObj);
            testCase.verifyEqual(o.lengthbusday,62,'');
            
            rangeObj = DateRange('1/1/2014','4/1/2015');
            o = ClockSimple.getInstance(rangeObj);
            testCase.verifyEqual(o.lengthbusday,314,'');
            
        end
        
        
        function testRunAndReset(testCase)
            rangeObj = DateRange('1/1/2015','4/1/2015');
            o = ClockSimple.getInstance(rangeObj);
            
            o.reset;
            testCase.verifyEqual(o.counter,0,'');
            testCase.verifyEqual(o.current,[],'');
            
            o.tick();
            testCase.verifyEqual(o.counter,1,'');
            testCase.verifyEqual(o.current,o.busdays(1),'');
            
            o.tick();
            o.tick();
            testCase.verifyEqual(o.current,o.busdays(3),'');
            
            o.reset();
            for iter =1 :o.lengthbusday
                o.tick();
                testCase.verifyEqual(o.current,o.busdays(iter));
            end
            testCase.verifyEqual(o.counter,o.lengthbusday,'');
            testCase.verifyTrue(o.isend == true,'');
        end
        
        

        
    end
    
end