classdef tdaterange < matlab.unittest.TestCase
    
    methods (Test)
% % R1 . accept human readable date format and store it 
% % internally as integers. verify that internall it is truely integers
% % R2 . only accept 2 string. if input is only one argument or no 
% % argument , throw eror

           function testaccept2inputAndConvert(testCase)
            
            
            o = DateRange('3/1/2015','3/9/2015');
            testCase.verifyTrue( isnumeric(o.startdate));
            testCase.verifyTrue( isnumeric(o.enddate));
            
            o = DateRange(0,0);
            testCase.verifyTrue( isnumeric(o.startdate));
            testCase.verifyTrue( isnumeric(o.enddate));
            
            
            o = DateRange(1,2);
            testCase.verifyTrue( isnumeric(o.startdate));
            testCase.verifyTrue( isnumeric(o.enddate));
            
            testCase.verifyError(@()DateRange(-1,2),'DateRange:InvalidInput',...
                '');
            testCase.verifyError(@()DateRange(3,2),'DateRange:InvalidInput',...
                '');
            testCase.verifyError(@()DateRange(3),'DateRange:InputNum',...
                '');
            testCase.verifyError(@()DateRange(),'DateRange:InputNum',...
                '');
        end
%  R3 can do comparison correctly , test equal and no equal
        function testCompare(testCase)
            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('2/1/2015','3/1/2015');
            o3 = DateRange('2/1/2015','4/1/2015');
            
            testCase.verifyTrue(o1==o2,'eq op failed');
            testCase.verifyFalse(o1==o3,'eq op failed');
            testCase.verifyTrue(o1~=o3,'eq op failed');
            
        end
% R4 can do minus correctly
        function testSameDayMinus(testCase)
           % same day minus should equal to no operation 
            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('2/1/2015','3/1/2015');
            o3 = o2 -o1;
            testCase.verifyEqual(o3,DateRange(inf,inf),'');
        end
        
        function testHolidayStartDateSmaller(testCase) 
            o1 = DateRange('1/2/2015','3/1/2015');
            o2 = DateRange('1/1/2015','3/1/2015');
            o3 = o2 -o1;
            
            
        end
 % test minor error scenarios        
        function testMinus(testCase)
            % start date 
            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('2/2/2015','3/2/2015');
            testCase.verifyError(@()o2-o1,'DateRange:MinusError','');
            
            % end date
            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('1/1/2015','2/20/2015');
            testCase.verifyError(@()o2-o1,'DateRange:MinusError','');
            

           
            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('1/1/2015','3/2/2015');   % bigger range 
            o3 = o2 -o1;
            testCase.verifyTrue(DateRange(inf,datenum('3/2/2015')) == o3,'');

            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('2/1/2015','3/5/2015');   % range on one end 
            o3 = o2 -o1;
            o4= DateRange('3/1/2015','3/5/2015');
            testCase.verifyTrue(o4 == o3,'');
            
            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('2/1/2015','3/2/2015');   % range on one end 
            o3 = o2 -o1;
            o4= DateRange(inf,datenum('3/2/2015'));
            testCase.verifyTrue(o4 == o3,'');
            
            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('1/1/2015','3/2/2015');   % range on one end 
            o3 = o2 -o1;
            testCase.verifyTrue(o3 == DateRange(inf,datenum('3/2/2015')),'');
        end
        
        % r6 , support today input string 
        function testTodayString(testCase)
            o1 = DateRange('3/1/2015','today');
            testCase.verifyTrue(o1.enddate == datenum(today),'');
            o1 = DateRange('3/1/2015','Today');
            testCase.verifyTrue(o1.enddate == datenum(today),'');
            
            o1 = DateRange('2/1/2015','3/1/2015');
            o2 = DateRange('2/1/2015','Today');   % range on one end 
            o3 = o2 -o1;
            o4= DateRange('3/1/2015','Today');
            testCase.verifyTrue(o4 == o3,'');
            
        end
    end
    
end