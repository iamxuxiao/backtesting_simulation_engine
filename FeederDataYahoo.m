classdef FeederDataYahoo < FeederData
    properties
       conn 
       daterange
       date 
       sym
    end
    methods
        function obj = FeederDataYahoo()
           obj.conn =  yahoo;
        end
        function setRequirement(obj,sym,timerangeobj)    
            obj.sym = sym;
           obj.daterange = timerangeobj;

        end
        function csvresult = fetch(obj)
           % depending on the timeranage obj 
           % if its DateRange(t1,t2)
           if ~isinf(obj.daterange.startdate)
               csvresult = fetch(obj.conn,obj.sym,obj.daterange.startdate,...
                   obj.daterange.enddate);
           elseif isinf(obj.daterange.startdate) &&...
                  isinf(obj.daterange.enddate)
                % same day extraction , op operation               
               csvresult=[];
           else
               % only fetch one day data
               csvresult = fetch(obj.conn,obj.sym, obj.daterange.enddate);
           end
            
           [m , n ] = size(csvresult);
           if m==1 || n==1
               % do not flip
           else
               % flip the matrix
               csvresult = flip(csvresult);
           end
        end
    end
    
    
end