classdef XMLManager < handle
    properties
        filname
    end
    
    methods(Access = private)
        function obj = XMLManager()
            
        end
    end
    
    
    methods(Static)
        function obj = getXMLManager()
            persistent localObj; 
            if isempty(localObj) || ~isvalid(localObj) 
                localObj = XMLManager();
            end
            obj = localObj; 
        end
        
        
    end
    methods
%         ID      = getID(obj,xmlfilename);
%         desc    = getDesc(obj,xmlfilename);
% %         data    = getPV(obj,data,xmlfilename);
%         data    = getPVV(obj,data,xmlfilename);
%         fileName = getFileName(obj,data,xmlfilename);
%         specdata = getDataFromText(obj,xmlfilename);
%         
%         elementSymCell  = getElementSym(obj,data,xmlfilename);
       
        resultStruct = xml2struct(obj,xmlfilename);
        
%         writeXML(obj,newdata,fileName);
%         docNode = writeID(obj,docNode,newdata);
%         docNode = writeDesc(obj,docNode,newdata);
%         docNode = writeFileName(obj,docNode,newdata);
%         docNode = writeTag(obj,docNode,cellData);
    end
     
    
end
        