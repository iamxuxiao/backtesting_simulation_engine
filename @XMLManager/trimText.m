function val = trimText( obj,oldStruct )



if(isfield(oldStruct,'Text'))
    % stopping
    val = oldStruct.Text ;
    
else
    % recursive
    if ~iscell(oldStruct) && isstruct(oldStruct)
        fields = fieldnames(oldStruct);
         [cmd2 val oldStruct]= trimIndividual(obj,fields,oldStruct);

    elseif iscell(oldStruct)
        for jiter = 1 : length(oldStruct)
                if isstruct(oldStruct{jiter})
                    fields = fieldnames(oldStruct{jiter});
                     [cmd2 val oldStruct]= trimIndividual_vector(obj,fields,oldStruct,jiter);

                end
        end
    else
        val = oldStruct;
        
    end
    
end

end

% old struct is local not , must pass out cmd 
function [cmd2 val oldStruct]= trimIndividual(obj,fields,oldStruct)

for iter = 1 : length(fields)
    cmd1 = strcat('val = trimText(obj, oldStruct.',fields{iter},');');
    eval(cmd1);
    if strcmp(fields{iter},'Text')
        cmd2 = strcat('oldStruct= val;');   
    else
        cmd2 = strcat('oldStruct.',fields{iter},'= val;');   
    end
        eval(cmd2);
       val = oldStruct;
end
end


function [cmd2 val oldStruct] = trimIndividual_vector(obj,fields,oldStruct,jiter)

for iter = 1 : length(fields)
    cmd1 = strcat('val = trimText(obj, oldStruct{jiter}.',fields{iter},');');
    eval(cmd1);
    %     oldStruct{jiter}.Text= val
    if strcmp(fields{iter},'Text')
        cmd2 = strcat('oldStruct{jiter}= val;');   
    else
        cmd2 = strcat('oldStruct{jiter}.',fields{iter},'= val;');   
    end
     eval(cmd2);
     val = oldStruct;
end
end