classdef ReportSimulation < handle
    properties
        fid
        filepath
        
        transcationTable ={}; 
    end
    methods(Access = private)
        function obj = ReportSimulation()
            datamanager = DataManager.getDataManager();
            config = datamanager.getData('config');
            obj.filepath = strcat(config.data.backtestingresult,...
                filesep,'report_simulation.txt');
            obj.fid = fopen(obj.filepath,'w');
            fprintf(obj.fid,'#+title: Report\n');
        end
%          fprintf(obj.fid,'stock      ireturn\n');
    end
    methods
        function delete(obj)
            fclose(obj.fid);
        end
        
        function listento(obj,publisher,eventname)
            fh = @obj.simulation_end_handler;
            addlistener(publisher,eventname,fh);
        end
        
        function simulation_end_handler(obj,publisher,evtobj)
           % publisher.engine.account contains the info
           account = publisher.engine.account ; 
           
          obj.write2report(account.stockname,account.investreturn, account.sharperatio);
%             obj.write2report(account.stockname,account.investreturn);
        end
        function write2report(obj,tick,ireturn,sharpe)
            fprintf(obj.fid,'|%s |%s|%s|\n',tick,num2str(ireturn),num2str(sharpe));

        end
        
        function writeTranscation2Report(obj,inputstring)
             fprintf(obj.fid,'%s\n',inputstring);
        end
    end
    methods(Static)
        function obj = getInstance()
            persistent localobj;
            if isempty(localobj) || ~isvalid(localobj)
                localobj = ReportSimulation();
            end
            obj = localobj;
        end
        
    end
end