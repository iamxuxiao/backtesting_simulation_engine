clear all;
clc
m = readtable('nasdaq_restore.csv');
cm = table2cell(m);
resultcell={};
counter=1;

for iter = 1:size(cm)
    row = cm(iter,:);
    marketcap_sym = row{4};
    containsB = strfind(marketcap_sym,'B');
    if ~isempty(containsB)
        cap = str2num(marketcap_sym(2:end-1));
        if cap >20
            resultcell{counter} = row;
            counter= counter+1;
        end
    end
end

resultcell=resultcell';
writetable(cell2table(resultcell),'nasdaq_above_20b.csv')