classdef DirectorDatafeed < handle
    properties
        builder
    end
    
    methods
        function work(obj)
           obj.builder.builddirectory(); 
           obj.builder.buildcsvdata();
        end
        
        
    end
end