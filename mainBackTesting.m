%% setup
% close all ; clear all; clear classes
format compact; format long

setup_path();    % developer hiding details 


%backtesting_config = ReaderConfig('.\config\backtesting_config.xml');
%backtesting_config = ReaderConfig('.\config\backtesting_config_ah.xml');
backtesting_config = ReaderConfig('.\config\backtesting_config_above_20b.xml');  % user
csvagg  =  AggregatorCSV(backtesting_config.data.backtestingsymset,...
    backtesting_config.data.csvstorage); %  user 




%% simulation
rangeObj = DateRange('1/1/2014','1/1/2015'); % user 

while ~csvagg.iterator.isDone()
    csvpath = csvagg.iterator.currentItem();
    a_stock = StockSimpleData(csvpath);
    a_stock.sym
    sim =  SimulatorBackTesting(rangeObj,a_stock);  % developer hook
    
    a_stock.addMA([5,20],'MA5_20'); % user, calculation done inside automatically 
    a_stock.addMACD('MACD');        % user  
    
    account = AccountSimple(5000,sim,a_stock.sym);  % user
    
    strategy_1 = StrategyTemplate(a_stock.indicators('MACD'), sim); % also listens to sim broadcast 
    % sim will boradcast at each time slice
    % developer should take care of this 
    
    strategy_2 = StrategyTemplate(a_stock.indicators('MA5_20'),sim);
    
   
    sim.strategymixer.addstrategy(strategy_1);  % user
    sim.strategymixer.addstrategy(strategy_2);  % user 
    
    

    sim.engine = ExecutionEngine(account, sim.strategymixer);  % developer 
    
    sim.startSimulation();%<------- user/developer 
    csvagg.iterator.nextItem(); % user/developer 
end




