classdef hObserver < handle
    properties
        mockprice
    end
    methods
        function handler(obj,publisher,evtobj)
            
            obj.mockprice = evtobj.price;
        end
    end
end