classdef Builder < handle
    methods(Abstract)
        builddirectory(obj);
        buildcsvdata(obj);
    end
end