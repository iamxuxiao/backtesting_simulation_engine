classdef BackTestingHarness < handle
    properties
        csvagg
        testToRun
        csvpath = {}
    end
    methods
        function setup_path(obj)
            disp('BackTestingHarness setup_path');
            setup_path();
        end
        function setup_testToRun(obj,testToRun)
            obj.testToRun = testToRun;
        end
        function setup_datasource(obj)
            config = obj.testToRun.config;
            obj.csvagg  =  AggregatorCSV(config.data.backtestingsymset,...
                config.data.csvstorage); %  user
            csvagg = obj.csvagg;
            while ~csvagg.iterator.isDone()
                csvpath = csvagg.iterator.currentItem();
                obj.csvpath{end+1} =csvpath;
                csvagg.iterator.nextItem(); % user/developer
            end
            obj.testToRun.csvpath = obj.csvpath;
        end
    end
end