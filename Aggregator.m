classdef Aggregator < handle
    properties
        filepath
        contentstable
        symbols
        totalnum
        iterator  
    end
   methods
       function obj =  Aggregator(path)
            obj.filepath = path ; 
            obj.contentstable = readtable(obj.filepath);
            obj.symbols = obj.contentstable{:,1};
            obj.totalnum = length(obj.symbols);
            
       end
   end
    methods(Abstract)
        iterObj = createIterator(obj);
    end
end
        