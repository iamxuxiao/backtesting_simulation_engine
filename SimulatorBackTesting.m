%> @file classDocumentationExample.m
%> @brief File used to show an example of class description
% ======================================================================
%> @brief Here we have a brief description of the class.
%
%> And here we can put some more detailed informations about the class.
% ======================================================================
classdef SimulatorBackTesting < Simulator
    properties
        %> This is a universal clock
        clock
        stock
        %> engine will own the account
        engine
        %strategy
        strategymixer
        log
    end
    events 
        simulation_end
    end
    events
        instructions_generate_signal
    end
    methods
        
        function obj = SimulatorBackTesting(range,stock)
            obj.clock = ClockSimple.getInstance(range);
            obj.stock = stock;
            
            % logger will listens to the "simulation_end" event
            try
            obj.log = ReportSimulation.getInstance();
            obj.log.listento(obj,'simulation_end');
            catch
                % not every unit test needs config and logger 
            end
            % if directory does not exist , create one 
            try
                datamanager = DataManager.getDataManager();
                config = datamanager.getData('config');
                dir = config.data.backtestingresult;
                if exist(dir,'dir') ~=7
                    mkdir(dir);
                end
            catch
                % not every simulation needs a logger
            end
            
            % continue initing 
            obj.strategymixer = StrategyMixer();        % default mixer develper
            
        end
        
        function addstrategy(obj,strategy)
           % add  to the mixer
           obj.strategymixer.addstrategy(strategy);
        end
        function results = startSimulation(obj)
            clock = obj.clock;
            clock.reset();
            for iter =1: clock.lengthbusday
                obj.clock.tick();
                obj.eventBlock(); % <------   
            end
            
            % at the end , notify that the simulation is ended
            obj.notify('simulation_end');
        end
        
        function eventBlock(obj) % all the events goes here
            % within one time slice
            %tickindex = obj.clock.counter ;
            % indicators notify
            % indicators registered at the engine
            obj.notify('instructions_generate_signal');
            % execuation engine listens to the signal and execute
        end
    end
end