classdef MVCManager < Context
    properties
       dataDictionary ; 
    end
    methods(Access = private)
        function obj = MVCManager()
            obj.dataDictionary = containers.Map();
        end
    end
    methods(Static)
        function obj = getMVCManager()
            persistent localObj; 
            if isempty(localObj) || ~isvalid(localObj) 
                localObj = MVCManager();
            end
            obj = localObj; 
        end
        
        
    end
    methods
        
        function data = getValidView(obj,ID)
            data = getData(obj,ID)   ;
            if ~ishghandle(data.hfig)
                cmd=ID;
                data = feval(cmd);
            end
            
        end
        
 function data = getData(obj,ID)    
            if isKey(obj.dataDictionary,ID)
                data = obj.dataDictionary(ID);
            else
                Logs.disp('[CroneTek::warning] object is not registered with MVC manager, MVC manager will create an object.');
                
                try
                    data = feval(ID); % ID is used as classname here 
                catch e
                   disp('singleton controller ! call getdata!');
                   try 
                      cmd = strcat(ID,'.getController');
                      data = feval(cmd);
                   catch ee
                      disp('not a controller ! call the ctor directly'); 
                      cmd=ID;
                      data = feval(cmd);
                   end
                end
            end
        end
     end

        
    
end
        