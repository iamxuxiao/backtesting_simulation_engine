classdef Context < handle
    
    methods
        
        
        function displayDictionary( obj )
            %DISPLAYDICTIONARY Summary of this function goes here
            %   Detailed explanation goes here
            disp('=== contents of data dictionary === ')
            msg = sprintf('size of dictionary = %i',length(obj.dataDictionary)); disp(msg);
            k = keys(obj.dataDictionary);
            
           
            for iter = 1 : length(k)
                disp('====================================');
                msg = sprintf('keys of dictionary : %s \n',k{iter}); disp(msg);
                obj.dataDictionary(k{iter})
                disp('====================================');
            end
        end
        
        
        function data = getData(obj,ID)    
            if iscellstr(ID) && isKey(obj.dataDictionary,ID{:})
                data = obj.dataDictionary(ID{:});
                if data == 0
                    Logs.disp('[CroneTek::warning] Data not registered, Data Dictionary will create on object ');
                end
            elseif isKey(obj.dataDictionary,ID)
                data = obj.dataDictionary(ID);
            else
                data = 0 ;
                Logs.disp('[CroneTek::warning] Data not registered, Data Dictionary will create on object ');
                data = feval(ID); % ID is used as classname here 
            end
        end
        
        function removeData(obj,ID)
            if isKey(obj.dataDictionary,ID)
                remove(obj.dataDictionary,ID);
            end
        end
        
        function boolResult = queryData(obj,ID)
            
            boolResult =  obj.dataDictionary.isKey(ID);
            
        end
        
        
        
        function register( obj,data,ID )
            if nargin == 3
                if iscellstr(ID)
                    expr = sprintf(' obj.dataDictionary(\''%s\'') = data;   ',ID{:});
                else
                    expr = sprintf(' obj.dataDictionary(\''%s\'') = data;   ',ID);
                end
            end
            
            % if registering in the form of ma.register(obj), this implies the ID
            % comes from obj.ID
            if nargin ==2
                expr = sprintf(' obj.dataDictionary(\''%s\'') = data;   ',data.ID);
            end
            
            
            eval(expr);
        end
        
        
    end
    
    
    
    
end