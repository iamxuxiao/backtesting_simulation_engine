classdef IteratorCSVSimple < handle 
    properties
        aggHandle
        counter
    end
    methods
        function obj =  IteratorCSVSimple(agg)
            obj.aggHandle = agg ; 
            obj.counter =1 ;
        end
        function csvfullname = first(obj)
           path = obj.aggHandle.csvstorageloc;
           csvfullname = strcat(path,obj.aggHandle.symbols{1});
        end
        function csvfullname = currentItem(obj)
            path = obj.aggHandle.csvstorageloc;
           csvfullname = strcat(path,obj.aggHandle.symbols{obj.counter});

        end
        function nextItem(obj)
            obj.counter = obj.counter + 1;
        end
        function boolval = isDone(obj)
           boolval = (obj.counter == obj.aggHandle.totalnum+1); 
        end
            
    end
end