classdef ControllerIndicators < BasicController
    properties
        
    end
    
    methods(Access = private)
        function obj = ControllerIndicators()
            obj = obj@BasicController('ControllerIndicators');
            obj.registerSelf();
        end
    end
    
    methods(Static)
        function obj = getController()
            persistent localObj;
            if isempty(localObj) || ~isvalid(localObj)
                localObj = ControllerIndicators();
            end
            obj = localObj;
            
        end
    end
    

end
