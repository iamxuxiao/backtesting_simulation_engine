classdef BasicController < handle
    properties
       ID 
       parentCtr
    end
    methods
        function obj = BasicController(ID)
            obj.ID = ID;
        end
        function registerSelf(obj)
            mvcManagerObj =  MVCManager.getMVCManager();
            mvcManagerObj.register(obj,obj.ID);
        end
    end
end
