classdef AggregatorExchange < Aggregator 
    properties

    end
    methods
        function obj = AggregatorExchange(path)
            obj = obj@Aggregator(path);
            obj.iterator = obj.createIterator();
        end
        function iterObj = createIterator(obj)
           iterObj = IteratorExchangeSimple(obj); 
        end
    end
end
    