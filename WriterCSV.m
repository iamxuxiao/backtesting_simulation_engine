classdef WriterCSV < Writer
    properties
        
        
    end
    methods
        function obj = WriterCSV()
            
        end
        function write(obj,csv,filename)
            % formting the data before write 
            % 1 the first col convert the time into offical string 
            % csvwrite(filename,csv); % bad precision 
            csv = unique(csv,'rows');
            dlmwrite(filename, csv, 'delimiter', ',', 'precision', 9,'-append'); 
            
            % after write regularize , remove duplication 
            csv_before = dlmread(filename);
            [csv_after ]= unique(csv_before,'rows');
            % make sure of the sequence
            csv_after = sortrows(csv_after,1); % sort rows according to 1st col
            if size(csv_before,1) ~=size(csv_after,1)
                % wipe out and write 
                dlmwrite(filename, csv_after, 'delimiter', ',', 'precision',9); 
            end
        end
    end
end
    