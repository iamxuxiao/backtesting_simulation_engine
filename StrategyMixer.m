classdef StrategyMixer <handle%< Strategy
   properties
       strategies  
       
   end
   events 
      buy %  event for the engine
      sell 
   end
    methods
        function obj = StrategyMixer()

        end
        function addstrategy(obj,individual)
            fh =@obj.generateSignal;
            % buy sell signal coming one buy one
            addlistener(individual,'buy',fh);
            addlistener(individual,'sell',fh);
        end
        function generateSignal(obj,publisher,evtobj)
            % simple redirection
      
            price = evtobj.price;
      
            % before writing special mixer
            % this mixer logic is shared by 
            % a. MA+MACD strategy
            % b. ROC+BOLLINGER strategy
            if strcmp(evtobj.EventName,'sell')   && ...
               strcmp(evtobj.source,'MA5_20')  ||   strcmp(evtobj.source,'lossmonitor')
                obj.notify('sell',EventBuySell(price,'mixer'));
            elseif strcmp(evtobj.EventName,'buy') && ...
               strcmp(evtobj.source,'MACD')       
                obj.notify('buy',EventBuySell(price,'mixer'));
            end
          
            % mixer logic for ROC+BOLLINGER
             if strcmp(evtobj.EventName,'sell')   &&   strcmp(evtobj.source,'ROCBOLLINGER') 
                obj.notify('sell',EventBuySell(price,'mixer'));
             elseif strcmp(evtobj.EventName,'buy')   &&   strcmp(evtobj.source,'ROCBOLLINGER') 
                obj.notify('buy',EventBuySell(price,'mixer'));
            end
        end
    end
end