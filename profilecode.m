  rangeObj = DateRange('1/1/2014','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            a_stock.addMACD('MACD');
            a_stock.calIndicators();
            
            strategy_1 = StrategyMACD(a_stock.indicators('MACD'));
            strategy_1.register(sim);  % sim will boradcast at each time slice
            
            sim.strategymixer = StrategyMixer();
            sim.strategymixer.addstrategy(strategy_1);
            
            
            sim.engine = ExecutionEngine(AccountSimple(5000),...
                sim.strategymixer);
            
            sim.startSimulation();%<-------
            account = sim.engine.account
%             testCase.verifyEqual(account.facevalue,5314);