classdef IndicatorMA_2< Indicators
    properties
        lag
        
        timeseries
        stock
    end
    methods
        function obj = IndicatorMA_2(lag,name,stock)
            obj = obj@Indicators(name);
            obj.lag = lag;
            obj.stock = stock;
        end
        
        function calculate(obj,tsobj)
            % % Date, Open, Hight, Low, Close,Vol, Adj Close
            obj.data = cell(length(obj.lag),1);
            for iter = 1: length(obj.lag)
                
                data = tsmovavg(tsobj.data(:,5),'s',...
                    obj.lag(iter),1);      
                % correct fowarding deviation
                data(2:end) = data(1:end-1);
                obj.data{iter}=data;
            end
           

        end
        
              
        function [result , price ]= query(obj)
            % querying the world clock for time
            clockobj = ClockSimple.getInstance();
            
            
            counter = clockobj.counter;
            MA5=obj.data{1};
            MA20=obj.data{2};
            t=counter;
            if MA5(t) > MA20(t) && MA5(t-1) >MA20(t-1) && MA5(t-2)<=MA20(t-2)
               result =  SignalSimple.BUY;
            elseif MA5(t) < MA20(t) && MA5(t-1) < MA20(t-1) && MA5(t-2)>=MA20(t-2)
               result =  SignalSimple.SELL;
            else
                result = SignalSimple.HOLD;   % turning point
            end
              
            price = obj.stock.data(counter,5);
        end
        
        function plotOnPanel(obj,panel)
            stock = obj.stock;
            axesobj  = axes('Parent', panel);
            plotOnItem(obj,axesobj);
        end
        
        function plotOnItem(obj,item)
            
            axesobj  = item;
            stock = obj.stock;
            h1 = line(stock.data(:,1),stock.data(:,5),'Parent',axesobj,'color','b');
            colorvec={'r','g','b','k','y'};
            for iter = 1: length(obj.data)
                h2 = line(stock.data(:,1),obj.data{iter},'Parent',axesobj,'color',colorvec{iter});
            end
            legend('stock','5','20');
            set(axesobj,'XTick',stock.data(1:30:end,1));
            set(axesobj,'XTickLabel',datestr(stock.data(1:30:end,1)));
            rotateXLabels(axesobj, 45 );
            
            hold on
            
            MA5=obj.data{1};
            MA20=obj.data{2};
            % find out the buy and sell signal 
            for t = 25: length(stock.data(:,5))
                if MA5(t) > MA20(t) && MA5(t-1) >MA20(t-1) && MA5(t-2)<=MA20(t-2) 
                    % buy signal 
                    text(stock.data(t,1),MA5(t),'B','Parent',axesobj);
                    plot(stock.data(t,1),MA5(t),'ro','markersize',8,'Parent',axesobj);
                elseif MA5(t) < MA20(t) && MA5(t-1) < MA20(t-1) && MA5(t-2)>=MA20(t-2)
                    % sell signal
                    text(stock.data(t,1),MA5(t),'S','Parent',axesobj);
                    plot(stock.data(t,1),MA5(t),'ro','markersize',8,'Parent',axesobj);
                else
                    % do nothing 
                end
            end 
            
            
            
        end
    end
end
