classdef IndicatorRoc<Indicators
    properties
        %     data
        proc
        stock

    end
    methods
        function  obj = IndicatorRoc(name,stock)
            obj = obj@Indicators(name);
            obj.stock = stock;
        end
   
        % query indicator what signal it should generate 
        % by default its after 12(windows size)
        function [result , price ]= query(obj)
            % querying the world clock for time
            clockobj = ClockSimple.getInstance();     
            counter = clockobj.counter;    
            if obj.proc(counter) > 0
                result =  SignalSimple.BUY;
            elseif obj.proc(counter) < 0
                result =  SignalSimple.SELL;
            else
                % no action
                result = SignalSimple.HOLD;   % turning point   
            end
            price = obj.stock.data(counter,5);
        end
        
        function calculate(obj,tsobj)
            % % Date, Open, Hight, Low, Close,Vol, Adj Close
            proc= prcroc(tsobj.data(:,5));
            obj.proc = proc;
        end
    end
end
