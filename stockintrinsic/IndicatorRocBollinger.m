classdef IndicatorRocBollinger<Indicators
    properties
        %     data
        mid
        upper
        lower
        stock
        proc
    end
    methods
        function  obj = IndicatorRocBollinger(name,stock)
            obj = obj@Indicators(name);
            obj.stock = stock;
        end
   
        % query indicator what signal it should generate 
        % by default its after 2o(windows size)
        function [result , price ]= query(obj)
            % querying the world clock for time
            clockobj = ClockSimple.getInstance();     
            counter = clockobj.counter;    
            price = obj.stock.data(counter,5);
            lowprice = obj.stock.data(counter,4);
            highprice = obj.stock.data(counter,3);
            
            if highprice > obj.upper(counter) && ...
                obj.proc(counter)>0
                result =  SignalSimple.BUY;
            elseif lowprice<obj.lower(counter)  && ...
                   obj.proc(counter)<0
                result =  SignalSimple.SELL;
            else
                % no action
                result = SignalSimple.HOLD;   % turning point   
            end
            
        end
        
        function calculate(obj,tsobj)
            % % Date, Open, High, Low, Close,Vol, Adj Close
            % windowsizedefault=20 for bollinger 
            [obj.mid, obj.upper, obj.lower] = bollinger(tsobj.data(:,5));
            obj.proc= prcroc(tsobj.data(:,5));
        end
    end
end
