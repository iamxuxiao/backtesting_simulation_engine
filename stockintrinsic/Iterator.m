classdef Iterator < handle
    methods(Abstract)
        itemObj = currentItem(obj);
        nextItem(obj);
        itemObj = first(obj);
        boolVal = isDone(obj);
    end
end