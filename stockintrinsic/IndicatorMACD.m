classdef IndicatorMACD<Indicators
    properties
        %     data
        macdvec
        nineperma
        stock
        diff
    end
    methods
        function  obj = IndicatorMACD(name,stock)
            obj = obj@Indicators(name);
            obj.stock = stock;
        end
        
        % Item can be panel or axes
        function plotOnItem(obj,item)
            stock = obj.stock;
            axesobj  = item;
            h1 = line(stock.data(:,1),stock.data(:,5),'Parent',axesobj,'color','b');
            set(axesobj,'XTick',stock.data(1:30:end,1));
            set(axesobj,'XTickLabel',datestr(stock.data(1:30:end,1)));
            rotateXLabels(axesobj, 45 );
            hold on
            % plot the indicators of buy and sell
            for t = 30: length(obj.diff)
                if obj.diff(t)>0  && obj.diff(t-1) <0
                    % buy signal
                    text(stock.data(t,1), stock.data(t,5),'B','Parent',axesobj);
                    plot(stock.data(t,1), stock.data(t,5),'kx','markersize',8,'Parent',axesobj);
                elseif obj.diff(t)<0 && obj.diff(t-1)>0
                    % sell signal
                    text(stock.data(t,1), stock.data(t,5),'S','Parent',axesobj);
                    plot(stock.data(t,1), stock.data(t,5),'kx','markersize',8,'Parent',axesobj);
                else
                    % do nothing
                end
            end
        end
        
        function plotOnPanel(obj,panel)
            stock = obj.stock;
            axesobj  = axes('Parent', panel);
            plotOnItem(obj,axesobj);
        end
        
        
        
        % only after 26 , things signal starts to showing
        function [result , price ]= query(obj)
            % querying the world clock for time
            clockobj = ClockSimple.getInstance();
            
            
            counter = clockobj.counter;    
            if obj.diff(counter) > 0
                result =  SignalSimple.BUY;
            elseif obj.diff(counter) < 0
                result =  SignalSimple.SELL;
            else
                % no action
                result = SignalSimple.HOLD;   % turning point   
            end
            price = obj.stock.data(counter,5);
        end
        
        function calculate(obj,tsobj)
            % % Date, Open, Hight, Low, Close,Vol, Adj Close
            % macdvec = ema12p - ema26p; using the default algorithm 
            [macdvec, nineperma]= macd(tsobj.data(:,5));
            obj.macdvec = macdvec;
            obj.nineperma = nineperma;
            obj.diff = macdvec - nineperma;
            
            % correct forwarding diviation
            % can not use data at current point 
            obj.nineperma(2:end) = nineperma(1:end-1);
            obj.diff(2:end) = macdvec(1:end-1) - nineperma(1:end-1);
        end
    end
end
