classdef Indicators < handle
    properties
        name
        data
    end
    methods
        function obj = Indicators(name)
            obj.name = name;
        end
        function plotOnPanel(obj,panel)
            axesobj  = axes('Parent',panel);
            stock = obj.stock;
            h1 = line(stock.data(:,1),stock.data(:,5),'Parent',axesobj,'color','b');
             h2 = line(stock.data(:,1),obj.data,'Parent',axesobj,'color','r');
            legend('stock',obj.name);
            set(axesobj,'XTick',stock.data(1:30:end,1));
            set(axesobj,'XTickLabel',datestr(stock.data(1:30:end,1)));
            rotateXLabels(axesobj, 45 );
        end
    end
    methods(Abstract)
        calculate(obj);
        
        
    end
end
