classdef DataManager < Context
    properties
       dataDictionary ; 
    end
    methods(Access = private)
        function obj = DataManager()
            obj.dataDictionary = containers.Map();
        end
    end
    methods(Static)
        function obj = getDataManager()
            persistent localObj; 
            if isempty(localObj) || ~isvalid(localObj) 
                localObj = DataManager();
            end
            obj = localObj; 
        end
        
        
    end
    methods
      
     
    end

        
    
end
        