classdef EventBuySell < event.EventData
   properties
       price
       source
   end
   methods 
       function obj = EventBuySell(price,source)
          obj.price = price; 
          obj.source = source ;
       end
   end
end