classdef StrategyLossMonitor < handle 
    properties
        account   % monitor the loss
        lossratio
        lastbuy_facevalue
    end
%     events
%        stop_loss % exe_engine will sell all to stop loss 
%     end
    events
        buy     % will never generate this however 
        sell    % equal to stop_loss signal to the mixer 
    end
    methods
        function obj = StrategyLossMonitor(account,lossratio)
            % listens to the time progress
            obj.account = account ;
            obj.lossratio = lossratio ;
            obj.lastbuy_facevalue =[];
        end
        
         function register(obj,engine)
            fh =@obj.generateSignal;
            addlistener(engine,'instructions_generate_signal',fh);
         end
        
         function generateSignal(obj,publisher,evt)
            clockobj = ClockSimple.getInstance();
            current = clockobj.current ;
            counter = clockobj.counter ;
            price = publisher.stock.data(counter,5);
             
            acc = obj.account;
            facevalue = acc.favevalueatpriceof(price);
             % if keep decreasing for 20% generate sell signal
             if acc.numofshares > 0 && ...
                facevalue >0 &&  strcmp(acc.lasttrade,'buy')
                if  isempty(obj.lastbuy_facevalue)
                 obj.lastbuy_facevalue = facevalue;
                elseif facevalue<obj.lastbuy_facevalue*(1-obj.lossratio)
                    obj.notify('sell',EventBuySell(price,'lossmonitor'));
                end
             end
             
             
         end 
    end
end 