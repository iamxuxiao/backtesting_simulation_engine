classdef BuilderSimple < Builder
    properties
        agghandle
        feeder
        inputdaterange
        writer
        log
        
    end
    methods
        function obj = BuilderSimple()
            
            datamanager = DataManager.getDataManager();
            config = datamanager.getData('config');
            obj.agghandle = AggregatorExchange(config.data.exchangesymbolfile);
            obj.feeder = FeederDataYahoo();
            obj.writer = WriterCSV();
            obj.log = ReportDataFeeder.getInstance();
        end
        function builddirectory(obj)
            % verify directory exist otherwise create one
            datamanager = DataManager.getDataManager();
            config = datamanager.getData('config');
            mkdir(config.data.csvstorage);
        end
        
        function buildcsvdata(obj)
            iterator = obj.agghandle.iterator;
            writer = obj.writer ;
            log = obj.log;
            log.write2report('builder simple');
            
            datamanager = DataManager.getDataManager();
            config = datamanager.getData('config');
            
            filepath = config.data.csvstorage;
            while ~iterator.isDone()
                sym = iterator.currentItem();
                filename = strcat(filepath,sym) ;
                
                existingdaterange = ReaderCSV.extractdaterange(filename);
                
                newdaterange = obj.inputdaterange - existingdaterange;
                
                
                obj.feeder.setRequirement(sym,newdaterange);
                flag = true;
                try
                    csv = obj.feeder.fetch();
                    % if successful write to disk
                    if ~isempty(csv)
                        writer.write(csv,filename);
                        report = sprintf('sym = %s obtained @ %s',sym,filename);
                        log.write2report(report,2);
                    else
                        report = sprintf('sym = %s same day , no op',sym);
                        log.write2report(report,2);
                    end
                catch e
                    report = sprintf('sym = %s failed',sym,99);
                    log.write2report(e.message,99);
                    log.write2report(e.identifier,99);
                    flag = false ;
                end
                disp( sprintf('fetching ith=%d\n',iterator.counter));
                
                iterator.nextItem();
            end
        end
    end
end