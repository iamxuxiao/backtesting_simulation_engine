classdef StockSimpleData < Stock
    properties
        sym
        data
        timerange
        fullpath
        indicators
        
    end
    methods
        function obj = StockSimpleData(csvfullpath)
            [obj.data,  obj.timerange]= ReaderCSV.extractfulldata(csvfullpath);
            [tmp obj.sym] = fileparts(csvfullpath) ;
            obj.fullpath = csvfullpath;
            obj.indicators = containers.Map();
        end
        
        function addMA(obj, lag,name) % add and calculatd 
            indicator = IndicatorMA_2(lag,name,obj);
            indicator.calculate(obj)
            obj.indicators(name) = indicator;
        end
        
        function addMACD(obj,name)
            indicator = IndicatorMACD(name,obj);
            indicator.calculate(obj)
            obj.indicators(name) = indicator;
        end
        
        function addRoc(obj,name)
            indicator = IndicatorRoc(name,obj);
            indicator.calculate(obj)
            obj.indicators(name) = indicator;
        end
        
         function addROCBollinger(obj,name)
            indicator = IndicatorRocBollinger(name,obj);
            indicator.calculate(obj)
            obj.indicators(name) = indicator;
        end

        
        function addBollinger(obj,name)
            indicator = IndicatorBollinger(name,obj);
            indicator.calculate(obj)
            obj.indicators(name) = indicator;
        end
        
        function calIndicators(obj)
% %             allkeys = obj.indicators.keys;
% %             for iter = 1:length(allkeys)
% %                 key = allkeys{iter};
% %                 indicator = obj.indicators(key);
% %                 indicator.calculate(obj) ;   % pass close price
% %             end
        end
    end
end

