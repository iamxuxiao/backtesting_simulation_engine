classdef ReaderConfig < Reader
    properties
        filename
        data
    end
    methods
        function obj = ReaderConfig(filename)
            obj.filename = filename;
            
            obj.data = obj.readConfig();
            obj.validate();
            datamanager = DataManager.getDataManager();
            datamanager.register(obj,'config');
            
               % conversion 
            obj.data.riskless= str2num(obj.data.riskless);
        end
        
        function data = readConfig(obj)
            xmlManager = XMLManager.getXMLManager();
            temp = xmlManager.xml2struct(obj.filename);
            temp = xmlManager.trimText(temp);
            data = temp.root;
            
         
            
        end
        
        function validate(obj)
           % append backslash if input forgots
           lastsym = obj.data.csvstorage(end);
           if ~strcmp(lastsym,filesep) 
              % append
              obj.data.csvstorage = strcat(obj.data.csvstorage,filesep);
           end
        end
    end



end 