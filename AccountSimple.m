classdef AccountSimple < Account
    properties
        position
        facevalue % stock face value
        numofshares
        cash
        
        numofbuy
        numofsell
        lasttrade
        initialamount
        stockname = []
        assetvalue
        
        transcationTable
        tableindex
    end
    properties(Dependent)
        sharperatio
        numoftrades
        investreturn
    end
    
    methods
        function num = get.numoftrades(obj)
            num = obj.numofbuy + obj.numofsell;
        end
        function sharpe = get.sharperatio(obj)
            
            datamanager = DataManager.getDataManager();
            config = datamanager.getData('config');
            riskless = config.data.riskless;
            returns = diff(obj.assetvalue)./obj.assetvalue(1:end-1);
            sharpe = (mean(returns) - riskless )/std(returns);
        end
        function investreturn = get.investreturn(obj)
            %investreturn = (obj.facevalue + obj.cash)/obj.initialamount -1;
            if ~isempty(obj.assetvalue)
                investreturn = obj.assetvalue(end)/obj.initialamount -1;
            else
                investreturn = (obj.facevalue + obj.cash)/obj.initialamount -1;
            end
        end
        
        function obj = AccountSimple(amount,sim,stockname)
            
            obj.cash = amount;
            obj.initialamount = amount;
            obj.position =  0 ;
            obj.facevalue = obj.cash;
            obj.numofshares = 0;
            obj.numofbuy = 0;
            obj.numofsell =0;
            obj.lasttrade='';
            if nargin ==2
                obj.register(sim);
            end
            if nargin==3
                obj.register(sim);
                obj.stockname = stockname;
            end
            obj.assetvalue=[];
            obj.transcationTable={};
            obj.tableindex=0;
        end
        
        function buyatpriceof(obj,price)
            numofshares_topurchase = floor(obj.cash/price);
            if numofshares_topurchase > 0
                obj.numofbuy = obj.numofbuy+1;
                obj.lasttrade='buy';
                obj.numofshares = obj.numofshares+numofshares_topurchase;
                
                % IO
                clockobj = ClockSimple.getInstance();
                displayString = sprintf('|%s | buy |%d | %s |@%s|',clockobj.stringdate,numofshares_topurchase, obj.stockname,num2str(price));
                reporter= ReportSimulation.getInstance();
                reporter.writeTranscation2Report(displayString);
                
                % internal table
                obj.tableindex = obj.tableindex+1;
                obj.transcationTable{obj.tableindex,1}= clockobj.stringdate;
                obj.transcationTable{obj.tableindex,2}= 'buy';
                obj.transcationTable{obj.tableindex,3}= numofshares_topurchase;
                obj.transcationTable{obj.tableindex,4}= obj.stockname;
                obj.transcationTable{obj.tableindex,5}= num2str(price);
            end
            obj.cash = obj.cash - numofshares_topurchase*price;
            obj.facevalue = obj.numofshares*price;
            obj.position = obj.numofshares;
            
            
        end
        
        function register(obj,sim)
            fh=@obj.updateAssetvalue;
            addlistener(sim,'instructions_generate_signal',fh);
        end
        % asset means total
        function  updateAssetvalue(obj,publisher,evtobj)
            clockobj = ClockSimple.getInstance();
            counter = clockobj.counter ;
            price = publisher.stock.data(counter,5);
            
            
            currentassetvalue = obj.cash + obj.numofshares*price;
            obj.assetvalue=[obj.assetvalue,currentassetvalue];
        end
        
        function facevalue = favevalueatpriceof(obj,price)
            %obj.facevalue = price*obj.numofshares ; % updating the
            %facevalue!??
            facevalue = price*obj.numofshares ;
        end
        
        function sellatpriceof(obj,price)
            % sell all
            obj.cash = obj.cash + obj.numofshares*price;
            previousshares = obj.numofshares;
            if obj.numofshares > 0
                obj.numofsell = obj.numofsell+1;
                obj.lasttrade = 'sell';
                
                clockobj = ClockSimple.getInstance();
                displayString = sprintf('|%s |sell |%d | %s |@%s|',clockobj.stringdate,previousshares,obj.stockname,num2str(price));
                reporter= ReportSimulation.getInstance();
                reporter.writeTranscation2Report(displayString);
                
                % internal table
                obj.tableindex = obj.tableindex+1;
                
                obj.transcationTable{obj.tableindex,1}= clockobj.stringdate;
                obj.transcationTable{obj.tableindex,2}= 'sell';
                obj.transcationTable{obj.tableindex,3}= obj.numofshares;
                obj.transcationTable{obj.tableindex,4}= obj.stockname;
                obj.transcationTable{obj.tableindex,5}= num2str(price);
            end
            obj.facevalue = 0 ;%obj.cash;
            obj.position =0;
            obj.numofshares=0;
            
        end
        %         function disp(obj)
        %           basicInfo=cell2table({obj.stockname,obj.numoftrades,obj.position,obj.initialamount,obj.assetvalue(end),obj.investreturn},...
        %               'VariableNames',{'Stock','NumOfTrades','Position','InitialValue','Asset','Return'})
        %
        %           transtable = cell2table(obj.transcationTable,...
        %                 'VariableName',{'Date','Type','Shares','Stock','Price'})
        %         end
        
        function transcation = getResults(obj)
            %               basicinfo=cell2table({obj.stockname,obj.numoftrades,obj.position,obj.initialamount,obj.assetvalue(end),obj.investreturn},...
            %               'VariableNames',{'Stock','NumOfTrades','Position','InitialValue','Asset','Return'});
            %
            transcation = cell2table(obj.transcationTable,...
                'VariableName',{'Date','Type','Shares','Stock','Price'});
            transcation.Asset = cell(size(obj.transcationTable,1),1);
            transcation.Asset{1}=obj.initialamount;
            transcation.Asset{end}=obj.assetvalue(end);
            
            transcation.Return = cell(size(obj.transcationTable,1),1);
            transcation.Return{end}=obj.investreturn;
        end
        
        
    end
end


