classdef FigurePropertiesTest < matlab.unittest.TestCase
 
    properties
        TestFigure
    end
 
    methods(TestMethodSetup)
        function createFigure(testCase)
              if ~exist('.\unittest\datafeed','dir')
                mkdir('.\unittest\datafeed');
            else
                rmdir('.\unittest\datafeed','s');
            end
        end
    end
 
    methods(TestMethodTeardown)
 function teardir(testCase)
            pause(3);
            if exist('.\unittest\datafeed','dir')
                rmdir('.\unittest\datafeed','s');
            end
        end
    end
 
    methods(Test)
 
        function defaultCurrentPoint(testCase)
             config = ReaderConfig('nasdaq_unittest.xml');
            directorfeed = DirectorDatafeed;
            simplebuilder = BuilderSimple();
            simplebuilder.inputdaterange = DateRange(config.data.startdate,config.data.enddate);
            directorfeed.builder = simplebuilder;
            directorfeed.work();
        end
 
     
 
    end
 
end