classdef ExecutionEngine < handle
   properties
       account
      
   end
   methods
       function obj = ExecutionEngine(account,strategymixer)
            obj.account = account;
            

            
            fh =@obj.handler_buy;
            addlistener(strategymixer,'buy',fh);
            
            fh =@obj.handler_sell;
            addlistener(strategymixer,'sell',fh);
            
       end
       
       
       function handler_buy(obj,strategymixer,evtobj)
           price = evtobj.price;
           obj.account.buyatpriceof(price);
       end
       

       
       function handler_sell(obj,strategymixer,evtobj)
           price = evtobj.price;
           obj.account.sellatpriceof(price);          
       end
   end
end
       
       
   