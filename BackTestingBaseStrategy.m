classdef BackTestingBaseStrategy < handle
   properties 
        config
        timerangeobj
        csvpath
        dataset
        initialAmout
        sim_results
   end
   methods
       function obj = BackTestingBaseStrategy() 
         disp('BackTestingBaseStrategy constructor');
       end

   end
   methods(Abstract)
        testSetup(obj);
        testTearDown(obj);
        testInit(obj);
   end
end