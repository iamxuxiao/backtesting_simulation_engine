classdef ModelIndicators < BasicModel
    properties
        
        stockdata 
  
        % MVC
        viewobj
        controllerobj
        
    end
    
    events 
        updateplot
    end


        
    
    methods  
        function obj = ModelIndicators()
            obj = obj@BasicModel('ModelIndicators');
            obj.registerSelf();       
            
            obj.viewobj = ViewIndicators();
            set(obj.viewobj.hfig,'Visible','off');
        end

        function setData(obj,stock)

          obj.stockdata = stock;
	  obj.notify('updateplot');
        end

    end
end

