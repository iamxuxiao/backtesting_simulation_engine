classdef BasicModel < handle
    properties
          ID            ;
    end
    
    methods
        function obj = BasicModel(ID)
            obj.ID = ID;
        end
          function registerSelf(obj)
            mvcManagerObj =  MVCManager.getMVCManager();
            mvcManagerObj.register(obj,obj.ID);
        end
       
    end
    
end

