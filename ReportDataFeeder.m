classdef ReportDataFeeder < handle 
    properties
        fid
    end
    methods(Access = private)
        function obj = ReportDataFeeder()
            obj.fid = fopen('report_feeder.txt','w');
        end
    end
    methods
        function delete(obj)
            fclose(obj.fid);
        end
          
        function write2report(obj,str,indent)
           if nargin == 2
               indent = 0 ;
           else
               switch indent
                   case 1
                        indent = '--' ; 
                   case 2 
                         indent = '----' ; 
                   case 3 
                         indent = '------' ; 
                   case 4
                         indent = '--------' ; 
                   case 99
                         indent = '--->';
               
               end
           end
           fprintf(obj.fid,'%s %s\n',indent,str); 
        end
    end
    methods(Static)
        function obj = getInstance()
           persistent localobj;
           if isempty(localobj) || ~isvalid(localobj)
              localobj = ReportDataFeeder(); 
           end
           obj = localobj;
        end
        
    end
end