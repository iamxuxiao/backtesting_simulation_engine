classdef StrategyMACD < Strategy
   properties
       indicator
       currentsignal   
   end
   events 
      buy     % signal for the mixer
      sell 
   end
    methods
        function obj = StrategyMACD(ind)
           obj.indicator = ind;
           obj.currentsignal = [];
        end
        function register(obj,engine)
             fh =@obj.generateSignal;
            addlistener(engine,'instructions_generate_signal',fh);
        end
        function generateSignal(obj,publisher,evt) 
            
            
            % generate new signal 
            % macdindicator and judgeMarketSignal are sharing 
            % the same clock
           [ newSignal,  price]= obj.indicator.query();
             
            % first time , set the signal 
            if isempty(obj.currentsignal) 
               if newSignal == SignalSimple.BUY 
                   obj.currentsignal = SignalSimple.BUY;
                   
               elseif newSignal == SignalSimple.SELL
                   obj.currentsignal = SignalSimple.SELL;
               else 
                   obj.currentsignal = SignalSimple.HOLD;
               end
            elseif obj.currentsignal == SignalSimple.HOLD
                obj.currentsignal = newSignal;
            end
           
           if obj.currentsignal== SignalSimple.SELL && ...
              newSignal == SignalSimple.BUY
%               disp('MACD before notify buy');
%               disp(price);
              obj.notify('buy',EventBuySell(price,'MACD'));
              obj.currentsignal = SignalSimple.BUY;
             
           elseif obj.currentsignal== SignalSimple.SELL   && ...
                 newSignal == SignalSimple.SELL
% %                % do nothing
% %                disp('MA CD hold');
           elseif  obj.currentsignal== SignalSimple.BUY  && ...
                   newSignal == SignalSimple.BUY
               % do nothing    
% %                disp('hold');
           elseif  obj.currentsignal== SignalSimple.BUY && ...
              newSignal == SignalSimple.SELL
%                disp('MACD before notify sell');
%                disp(price);
               obj.notify('sell',EventBuySell(price,'MACD')); 
               obj.currentsignal = SignalSimple.SELL;
               
           end
            
        end
    end
end