classdef tsimulator < matlab.unittest.TestCase
    
    % commom
    % %                testCase.verifyThat(account.investreturn,...
    % %                  matlab.unittest.constraints.IsEqualTo(0.0055,...
    % %                 'Within', matlab.unittest.constraints.AbsoluteTolerance(0.001)));
    
    
    
    methods(Test)
        function tindicators(testCase)
            rangeObj = DateRange('1/1/2014','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            a_stock.addMA([5,20],'MA5_20');
            
            % make sure MA5 and MA2o are calculted
            testCase.verifyEqual(length(a_stock.indicators('MA5_20').data),2,'');
        end
        
        
        function tgui(testCase)
            rangeObj = DateRange('1/1/2015','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AMZN');
            a_stock.addMA([5,20], 'MA5_20');
            a_stock.addMACD('MACD');
            
            modelobj = ModelIndicators();
            modelobj.setData(a_stock)
            f = modelobj.viewobj.hfig;
            set(f,'visible','off');
            
        end
        
        function tEnumSignale(testCase)
            % test enum compare
            buy = SignalSimple.BUY;
            sell = SignalSimple.SELL;
            hold = SignalSimple.HOLD;
            
            testCase.verifyTrue(buy~=sell,'');
            testCase.verifyTrue(buy~=hold,'');
            testCase.verifyTrue(hold~=sell,'');
            
            buy1 = SignalSimple.BUY;
            sell1 = SignalSimple.SELL;
            hold1 = SignalSimple.HOLD;
            
            testCase.verifyTrue(buy1 == buy,'');
            testCase.verifyTrue(sell1 == sell,'');
            testCase.verifyTrue(hold1 == hold,'');
        end
        
        function testLossMonitor(testCase)
            % find a stock that goes down trend
            % issue a signal buy at the peak
            % turn off the macd and ma stratgy. so no warnnig from strategy
            % as time goes by , make sure the loss monitor come into play
            rangeobj = DateRange('9/25/2014','4/10/2015');
            conn = yahoo;
            baba_csv = fetch(conn,'BABA',rangeobj.startdate,...
                rangeobj.enddate);
            tempfilename = strcat('.\derived\baba_csv.csv');
            if exist(tempfilename,'file') ==2
                delete(tempfilename)
            end
            writer = WriterCSV;
            writer.write(baba_csv,tempfilename);  % temp csv to construct the stockdata
            a_stock = StockSimpleData(tempfilename);
            sim =  SimulatorBackTesting(rangeobj,a_stock);
            
            strategy_mock = StrategyMock({'11/12/2014'},{'2/26/2015'});
            strategy_mock.register(sim);
            
            account = AccountSimple(5000, sim);
            strategy_loss_monitor = StrategyLossMonitor(account,0.1);
            strategy_loss_monitor.register(sim);
            
            
            sim.strategymixer = StrategyMixerMock(); % simple pass through
                                                     % replace the default 
            sim.strategymixer.addstrategy(strategy_mock);
            sim.strategymixer.addstrategy(strategy_loss_monitor);
            
            
            
            sim.engine = ExecutionEngine(account,...
                sim.strategymixer);
            sim.startSimulation();
            testCase.verifyTrue(account.numoftrades ==2);
            testCase.verifyTrue(account.investreturn<0 );
            testCase.verifyTrue(account.numofshares==0 );
            testCase.verifyTrue(account.cash>4200 );
        end
        
        
        function testStrategyMock(testCase)  % run time fetch data
            % test execu of Mock Strategu
            rangeobj = DateRange('9/25/2014','4/10/2015');
            conn = yahoo;
            baba_csv = fetch(conn,'BABA',rangeobj.startdate,...
                rangeobj.enddate);
            tempfilename = strcat('.\derived\baba_csv.csv');
            if exist(tempfilename,'file') ==2
                delete(tempfilename)
            end
            writer = WriterCSV;
            writer.write(baba_csv,tempfilename);  % temp csv to construct the stockdata
            a_stock = StockSimpleData(tempfilename);
            sim =  SimulatorBackTesting(rangeobj,a_stock);
            
            strategy_mock = StrategyMock({'11/12/2014'},{'2/26/2015'}); % by at , sell at
            strategy_mock.register(sim);
            
            sim.strategymixer = StrategyMixerMock();
            sim.strategymixer.addstrategy(strategy_mock);
            account = AccountSimple(5000,sim);
            sim.engine = ExecutionEngine(account,...
                sim.strategymixer);
            sim.startSimulation();
            testCase.verifyTrue(account.numoftrades ==2);
            testCase.verifyTrue(account.investreturn<0 );
            testCase.verifyTrue(account.numofshares==0 );
            
        end
        
        function simplesimulatorTesting(testCase)
            % simple simulation
            rangeObj = DateRange('1/1/2015','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            a_stock.addMACD('MACD');
            
            
            strategy_1 = StrategyTemplate(a_stock.indicators('MACD'),sim);
            
%             sim.strategymixer = StrategyMixer();
            sim.strategymixer.addstrategy(strategy_1);
            
            sim.engine = ExecutionEngine(AccountSimple(5000,sim),...
                sim.strategymixer);
            sim.startSimulation();%<-------
            
            clock = sim.clock;
            testCase.verifyEqual(clock.counter,clock.lengthbusday,'');
            testCase.verifyTrue(clock.isend == true,'');
        end
        
        
        
        function simpleBuySellExecution(testCase)
            rangeObj = DateRange('1/1/2015','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            a_stock.addMACD('MACD');
            
            simmock = SimulatorMock;
            strategy = StrategyTemplate(a_stock.indicators('MACD'),simmock);
            account = AccountSimple(5000,simmock);
            engine = ExecutionEngine(account,strategy) ;
            
            strategy.notify('sell',EventBuySell(10,'TEST')); % no position to sell
            testCase.verifyEqual(account.cash,5000);
            testCase.verifyEqual(account.numoftrades,0);
            
            account.cash=0;
            strategy.notify('buy',EventBuySell(99,'TEST')); % no money to buy
            testCase.verifyEqual(account.cash,0);
            testCase.verifyEqual(account.numoftrades,0);
        end
        
        function longerExecuationMACD(testCase)
            rangeObj = DateRange('1/1/2014','4/1/2015');
            
            % for riskless
            backtesting_config = ReaderConfig('.\config\backtesting_config_above_20b.xml');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            a_stock.addMACD('MACD');
            strategy_1 = StrategyTemplate(a_stock.indicators('MACD'),sim);
            
            account = AccountSimple(5000, sim);
            sim.engine = ExecutionEngine(account,...
                strategy_1);
            
            
            
            sim.startSimulation();%<-------
            account = sim.engine.account;
            testCase.verifyThat(account.facevalue,...
                matlab.unittest.constraints.IsEqualTo(5416,...
                'Within', matlab.unittest.constraints.AbsoluteTolerance(10)));
            
            account = sim.engine.account;
            
            testCase.verifyThat(account.sharperatio,...
                matlab.unittest.constraints.IsEqualTo( -3.344,...
                'Within', matlab.unittest.constraints.AbsoluteTolerance(0.01)));
            
            
        end
        
        function longerExecuationMA520(testCase)
            rangeObj = DateRange('1/1/2014','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            a_stock.addMA([5,20],'MA5_20');
            
            
            strategy_1 = StrategyTemplate(a_stock.indicators('MA5_20'),sim);
            
            
            sim.engine = ExecutionEngine(AccountSimple(5000,sim),...
                strategy_1); % no mixer , give it directly to the engine
            
            sim.startSimulation();%<-------
            account = sim.engine.account;
            testCase.verifyThat(account.facevalue,...
                matlab.unittest.constraints.IsEqualTo(4904,...
                'Within', matlab.unittest.constraints.AbsoluteTolerance(10)));
            
            %             testCase.verifyEqual(account.facevalue,4829.49);
            
        end
        
        
        function mixingTwo(testCase)
            
            rangeObj = DateRange('1/1/2014','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            
            a_stock.addMA([5,20],'MA5_20');
            a_stock.addMACD('MACD');
            
            
            strategy_1 = StrategyTemplate(a_stock.indicators('MACD'),sim);
            strategy_2 = StrategyTemplate(a_stock.indicators('MA5_20'),sim);
            
            
            
            sim.strategymixer = StrategyMixer();
            sim.strategymixer.addstrategy(strategy_1);
            sim.strategymixer.addstrategy(strategy_2);
            
            sim.engine = ExecutionEngine(AccountSimple(5000,sim),...
                sim.strategymixer);
            sim.startSimulation();%<-------
            account = sim.engine.account;
            testCase.verifyThat(account.cash,...
                matlab.unittest.constraints.IsEqualTo(401.4,...
                'Within', matlab.unittest.constraints.AbsoluteTolerance(10)));
            
            %             testCase.verifyEqual(account.facevalue,5314);
            testCase.verifyEqual(account.numoftrades,19);
        end
        
        function mixingThree(testCase)
            % make sure stop loss monitor works
            % by setting stop loss ratio to be really small
            
            rangeObj = DateRange('1/1/2014','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            a_stock.addMA([5,20],'MA5_20');
            a_stock.addMACD('MACD');
            
            
            strategy_1 = StrategyTemplate(a_stock.indicators('MACD'),sim);
            strategy_2 = StrategyTemplate(a_stock.indicators('MA5_20'),sim);
            
            
            
%             sim.strategymixer = StrategyMixer();
            sim.strategymixer.addstrategy(strategy_1);
            sim.strategymixer.addstrategy(strategy_2);
            
            
            account = AccountSimple(5000,sim);
            strategy_loss_monitor = StrategyLossMonitor(account,0.1);
            strategy_loss_monitor.register(sim);
            sim.strategymixer.addstrategy(strategy_loss_monitor);
            
            sim.engine = ExecutionEngine(account,...
                sim.strategymixer);
            sim.startSimulation();%<-------
            account = sim.engine.account;
            testCase.verifyThat(account.investreturn,...
                matlab.unittest.constraints.IsEqualTo(0.0847,...
                'Within', matlab.unittest.constraints.AbsoluteTolerance(0.01)));
        end
        
        
        function iteratingMultipStockUsingSameStrategy(testCase)
            rangeObj = DateRange('1/1/2014','1/1/2015'); % 1 year
            
            backtesting_config = ReaderConfig('.\config\backtesting_config.xml');
            csvagg  =  AggregatorCSV(backtesting_config.data.backtestingsymset,...
                backtesting_config.data.csvstorage); % name agg
            
            while ~csvagg.iterator.isDone()
                csvpath = csvagg.iterator.currentItem();
                a_stock = StockSimpleData(csvpath);
                
                sim =  SimulatorBackTesting(rangeObj,a_stock);
                a_stock.addMA([5,20],'MA5_20');
                a_stock.addMACD('MACD');
                
                
                strategy_1 = StrategyTemplate(a_stock.indicators('MACD'),sim);
                
                %                 strategy_2 = StrategyMA(a_stock.indicators('MA5_20'));
                %                 strategy_2.register(sim);
                
                
%                 sim.strategymixer = StrategyMixer();
                sim.strategymixer.addstrategy(strategy_1);
                %                 sim.strategymixer.addstrategy(strategy_2);
                
                account = AccountSimple(5000,sim,a_stock.sym);
                sim.engine = ExecutionEngine(account, sim.strategymixer);
                sim.startSimulation();%<-------
                csvagg.iterator.nextItem();
            end
            
            
        end
        
        function mixingTwoAmazon(testCase)
            
            rangeObj = DateRange('1/1/2014','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AMZN');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            a_stock.addMA([5,20],'MA5_20');
            a_stock.addMACD('MACD');
            
            
            strategy_1 = StrategyTemplate(a_stock.indicators('MACD'),sim);
            strategy_2 = StrategyTemplate(a_stock.indicators('MA5_20'),sim);
            
            
            
%             sim.strategymixer = StrategyMixer();
            sim.strategymixer.addstrategy(strategy_1);
            sim.strategymixer.addstrategy(strategy_2);
            
            sim.engine = ExecutionEngine(AccountSimple(5000,sim),...
                sim.strategymixer);
            sim.startSimulation();%<-------
            account = sim.engine.account;
            testCase.verifyThat(account.cash,...
                matlab.unittest.constraints.IsEqualTo(6136,...
                'Within', matlab.unittest.constraints.AbsoluteTolerance(10)));
            
            %             testCase.verifyEqual(account.cash,6435.81);
            testCase.verifyEqual(account.numoftrades,14);
            %             testCase.verifyEqual(account.investreturn,1435.81/5000);
        end
        
        function testLogger(testCase)
            rangeObj = DateRange('1/1/2014','1/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AMZN');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            a_stock.addMA([5,20],'MA5_20');
            a_stock.addMACD('MACD');
            strategy_1 = StrategyTemplate(a_stock.indicators('MACD'),sim);
            strategy_2 = StrategyTemplate(a_stock.indicators('MA5_20'),sim);
            sim.strategymixer.addstrategy(strategy_1);
            sim.strategymixer.addstrategy(strategy_2);
            account = AccountSimple(5000,sim,a_stock.sym);
            sim.engine = ExecutionEngine(account,...
                sim.strategymixer);
            
            % delete the logger 
            sim.startSimulation();%<-------
            
        end
        
        function testTranscationReport(testCase)
            rangeObj = DateRange('1/1/2014','1/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AMZN');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            a_stock.addMA([5,20],'MA5_20');
            a_stock.addMACD('MACD');
            strategy_1 = StrategyTemplate(a_stock.indicators('MACD'),sim);
            strategy_2 = StrategyTemplate(a_stock.indicators('MA5_20'),sim);
            sim.strategymixer.addstrategy(strategy_1);
            sim.strategymixer.addstrategy(strategy_2);
            account = AccountSimple(5000,sim,a_stock.sym);
            sim.engine = ExecutionEngine(account,...
                sim.strategymixer);
            
            % delete the logger
            sim.startSimulation();%<-------
            
            
        end
        function testSimpleMixer(testCase)
            rangeObj = DateRange('1/1/2014','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            a_stock.addMACD('MACD');
            simmock = SimulatorMock;
            strategy_1 =  StrategyTemplate(a_stock.indicators('MACD'),simmock);
            
             strategymixer = StrategyMixer();
            strategymixer.addstrategy(strategy_1);
            
            
            observer = hObserver();
            addlistener(strategymixer,'buy',@observer.handler);
            addlistener(strategymixer,'sell',@observer.handler);
            strategy_1.notify('buy',EventBuySell(20,'MACD'));
            
            testCase.verifyEqual(observer.mockprice, 20,'');
            
            strategy_1.notify('sell',EventBuySell(10,'MA5_20'));
            testCase.verifyEqual(observer.mockprice, 10,'');
            
        end
        
        
        
        
        % testing the calculation of returns
        function testAccountDisplayReturn(testCase)
            account = AccountSimple(5000);
            account.buyatpriceof(400);
            account.sellatpriceof(500);
            testCase.verifyEqual(account.investreturn , 0.24,'');
        end
        
        
        
        % the complex system started with well designed
        % smallest pieces of code , and grows
        function  testSimpleAccountOperation(testCase)
            account = AccountSimple(5000); % new account
            account.buyatpriceof(500);
            testCase.verifyTrue(account.cash==0);
            testCase.verifyTrue(account.facevalue==5000);
            testCase.verifyTrue(account.numofshares==10);
            
            fv = account.favevalueatpriceof(480);
            testCase.verifyTrue(fv==4800);
            
            account.sellatpriceof(490);
            testCase.verifyTrue(account.cash==4900);
            testCase.verifyTrue(account.facevalue==0);
            testCase.verifyTrue(account.numofshares==0);
            testCase.verifyEqual(account.numoftrades,2);
            testCase.verifyEqual(account.numofbuy,1);
            testCase.verifyEqual(account.numofsell,1);
            
            account = AccountSimple(5000); % new account
            account.buyatpriceof(47);
            testCase.verifyTrue(account.cash==18);
            testCase.verifyTrue(account.numofshares==106);
            testCase.verifyTrue(account.facevalue==4982);
            
            fv = account.favevalueatpriceof(45.6);
            testCase.verifyTrue(fv==4833.6);
            
            account.sellatpriceof(40);
            testCase.verifyTrue(account.numofshares==0);
            testCase.verifyTrue(account.cash==4258);
            testCase.verifyEqual(account.numoftrades,2);
            
            account = AccountSimple(0.1); % new account
            account.buyatpriceof(500);
            testCase.verifyTrue(account.numofshares==0);
            testCase.verifyTrue(account.cash==0.1);
            testCase.verifyEqual(account.numoftrades,0);
        end
        
        
        function tharness_integrationTest(testCase) 
            testobj = backtestMACD
            runbacktestsuite(testobj);
            results = testobj.sim_results;
               testCase.verifyThat(results.Return{end},...
                matlab.unittest.constraints.IsEqualTo(0.19714,...
                'Within', matlab.unittest.constraints.AbsoluteTolerance(0.001)));

        end
        
        function tbollingroc(testCase)
            rangeObj = DateRange('1/1/2014','4/1/2015');
            a_stock = StockSimpleData('.\resources\data_backtest\AAPL');
            sim =  SimulatorBackTesting(rangeObj,a_stock);
            
            a_stock.addROCBollinger('ROCBOLLINGER');
             
            strategy_1 = StrategyTemplate(a_stock.indicators('ROCBOLLINGER'),sim);        
            sim.addstrategy(strategy_1);

%           
            account = AccountSimple(5000,sim);
            sim.engine = ExecutionEngine(account,...
                sim.strategymixer);
            sim.startSimulation();
            account = sim.engine.account
            testCase.verifyThat(account.investreturn,...
                matlab.unittest.constraints.IsEqualTo(0.02,...
                'Within', matlab.unittest.constraints.AbsoluteTolerance(0.01)));
%         
            testCase.verifyEqual(account.numoftrades,7);
        end
    end
end
