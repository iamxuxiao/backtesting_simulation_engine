classdef ClockSimple < Clock
   properties
      busdays
      startdate
      enddate
      counter
      lengthbusday
      current
   end
   properties(Dependent)
      stringdate 
   end
   methods
       function stringdate = get.stringdate(obj)
          stringdate =  datestr(obj.busdays(obj.counter));
       end
   end
    methods(Access = private)
       function obj = ClockSimple(rangeobj)
          tmpStartDate = rangeobj.startdate;
          tmpEndDate = rangeobj.enddate;
          tmpDays = [tmpStartDate:1:tmpEndDate];
          obj.busdays = tmpDays( isbusday( tmpDays));
          obj.startdate = obj.busdays(1);
          obj.enddate = obj.busdays(end);
          obj.lengthbusday = length(obj.busdays);
          obj.counter =1;
       end
    end
    
    methods(Static)
        function obj = getInstance(rangeobj)
            persistent localObj;
            if isempty(localObj) || ~isvalid(localObj)
                if nargin==1
                    localObj = ClockSimple(rangeobj);
                else
                    ME = MException('Clock:init',...
                    'Clock not initialized yet');
                    throw(ME);
                end
            elseif ~isempty(localObj) && nargin==1
               % clear previous one and return a new obj with new range 
                localObj = ClockSimple(rangeobj);
            end
            obj = localObj;
        end
        
    end
    
   methods
 
      
       function reset(obj)
           obj.counter = 0;
           obj.current = [];
       end
       
       function tick(obj)
           obj.counter = obj.counter + 1;
           obj.current = obj.busdays(obj.counter);
       end
       
       function result = isend(obj)
            if obj.counter == length(obj.busdays)
               result = true; 
            else
               result = false;
            end
       end 
   end
end