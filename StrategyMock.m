classdef StrategyMock < Strategy
    properties
         indicator
        currentsignal
        
        buyat
        sellat
    end
    events
        buy     % signal for the mixer
        sell
    end
    methods
        function obj = StrategyMock(buyat,sellat) % input as datanum array
            obj.indicator = [];
            obj.currentsignal = [];
            if ~isempty(buyat)
                obj.buyat = datenum(buyat);       % cell array input
                                                  % input format '11/12/2014'
            
            else 
                obj.buyat =[];
            end
            if ~isempty(sellat)
                obj.sellat = datenum(sellat);
                
            else
                obj.sellat=[];
            end
        end
        function register(obj,engine)
            fh =@obj.generateSignal;
            addlistener(engine,'instructions_generate_signal',fh);
        end
        function generateSignal(obj,publisher,evt)
            % query the clock 
            % if the clock counter == any buyat or sellat 
            % send the signal 
            % querying the world clock for time
            clockobj = ClockSimple.getInstance();
            current = clockobj.current ;
            counter = clockobj.counter ;
            price = publisher.stock.data(counter,5);
            if find(obj.buyat == current)    
%                 price 
                obj.notify('buy',EventBuySell(price,'MOCK'));
            end
            
            if find(obj.sellat ==current)
%                 price
                obj.notify('sell',EventBuySell(price,'MOCK'));
            end
            
            
        end
    end
end