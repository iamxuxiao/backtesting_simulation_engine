classdef backtestMACD < BackTestingBaseStrategy
    methods
        function testSetup(obj)
            % place holder nothing at this moment 
        end
        function testTearDown(obj)
            % place holder nothing at this moment 
        end
    end
    
    methods
        function testInit(obj)
            % read config
            obj.config =  ReaderConfig('.\config\backtesting_config_above_20b.xml');
            
            % setup time range object
            obj.timerangeobj = DateRange('1/1/2014','1/1/2015'); % user
            
            % setup account amount
            obj.initialAmout=5000;
        end
        
        function csvpath = whichDataToSimulate(obj)
            csvpath = {obj.csvpath{1}}; % must be a cell
        end
        
        % harness will call this function, one stock at a time
        function testStrategyOnStock(obj,a_stock,account,sim)
            fprintf('backtesting stock %s\n',a_stock.sym);
            a_stock.addMA([5,20],'MA5_20'); 
            a_stock.addMACD('MACD');        
                
            strategy_1 = StrategyTemplate(a_stock.indicators('MACD'), sim); 
            strategy_2 = StrategyTemplate(a_stock.indicators('MA5_20'),sim);
            % strategy listens to sim's broadcast 
 
            sim.addstrategy(strategy_1);  
            sim.addstrategy(strategy_2); 

            sim.startSimulation();
            sim_results = account.getResults()
            
            % optional for testing
            obj.sim_results = sim_results;
        end
        
    end
end