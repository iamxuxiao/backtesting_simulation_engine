classdef ReaderCSV < Reader
    methods(Static)
        function existingrange=extractdaterange(filename)   
            try
                csv = dlmread(filename);
                existingrange = DateRange(csv(1,1),csv(end,1));
            catch
                existingrange = DateRange(0,0);  % all zero as input   
            end
        end
        
        function [data , existingrange] = extractfulldata(filename)
            try
                data = dlmread(filename);
                existingrange = DateRange(data(1,1),data(end,1));
            catch
               data =[];
               existingrange= DateRange(0,0);
            end
        end
    end
end
    