classdef StrategyTemplate < Strategy
    
    properties
        indicator
        currentsignal
        name
    end
    events
        buy     % signal for the mixer
        sell
    end
    methods
        function obj = StrategyTemplate(ind,sim)
            obj.indicator = ind;
            obj.currentsignal = [];
            obj.name = ind.name;
            obj.register(sim);
        end
        function register(obj,engine)
            fh =@obj.generateSignal;
            addlistener(engine,'instructions_generate_signal',fh);
        end
        function generateSignal(obj,publisher,evt)
            % generate new signal
            % macdindicator and judgeMarketSignal are sharing
            % the same clock
            % every tick time, indicator is queried 
            [ newSignal,  price]= obj.indicator.query();
            
            % first time , set the signal
            if isempty(obj.currentsignal)
                if newSignal == SignalSimple.BUY
                    obj.currentsignal = SignalSimple.BUY;
                    
                elseif newSignal == SignalSimple.SELL
                    obj.currentsignal = SignalSimple.SELL;
                else
                    obj.currentsignal = SignalSimple.HOLD;
                end
            elseif obj.currentsignal == SignalSimple.HOLD
                obj.currentsignal = newSignal;
            end
            % should just pass along !!!!
            if obj.currentsignal== SignalSimple.SELL && ...
                    newSignal == SignalSimple.BUY
                % buy
                obj.notify('buy',EventBuySell(price,obj.name));
                obj.currentsignal = SignalSimple.BUY;
                
            elseif obj.currentsignal== SignalSimple.SELL   && ...
                    newSignal == SignalSimple.SELL
                % do nothing
                obj.notify('sell',EventBuySell(price,obj.name));
                obj.currentsignal = SignalSimple.SELL;
            elseif  obj.currentsignal== SignalSimple.BUY  && ...
                    newSignal == SignalSimple.BUY
                % do nothing
                obj.notify('buy',EventBuySell(price,obj.name));
                obj.currentsignal = SignalSimple.BUY;
            elseif  obj.currentsignal== SignalSimple.BUY && ...
                    newSignal == SignalSimple.SELL
                % sell
                obj.notify('sell',EventBuySell(price,obj.name));
                obj.currentsignal = SignalSimple.SELL;
            end
            
        end
    end
end
