classdef AggregatorCSV < Aggregator 
    properties
        csvstorageloc
    end
    methods
        function obj = AggregatorCSV(path,csvstroage)
            obj = obj@Aggregator(path);
            obj.csvstorageloc = csvstroage;
            obj.iterator = obj.createIterator();
        end
        function iterObj = createIterator(obj)
           iterObj = IteratorCSVSimple(obj); 
        end
    end
end
    