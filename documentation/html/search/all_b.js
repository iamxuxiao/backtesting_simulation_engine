var searchData=
[
  ['second_5fproperty',['second_property',['../classclassDocumentationExample.html#a72f28f996574c2d17d1ae273b51bb961',1,'classDocumentationExample']]],
  ['secondevent',['SecondEvent',['../classclassDocumentationExample.html#a7dc3cbfe699268a7cca9ae9c8f9544b9adc054405d657bf15ceb8e94cb185dfe4',1,'classDocumentationExample']]],
  ['signalsimple',['SignalSimple',['../classSignalSimple.html',1,'']]],
  ['simulator',['Simulator',['../classSimulator.html',1,'']]],
  ['simulatorbacktesting',['SimulatorBackTesting',['../classSimulatorBackTesting.html',1,'SimulatorBackTesting'],['../classSimulatorBackTesting.html#ae07731665a3452235079dceb86d2d2dd',1,'SimulatorBackTesting::SimulatorBackTesting()']]],
  ['stock',['Stock',['../classStock.html',1,'']]],
  ['stocksimpledata',['StockSimpleData',['../classStockSimpleData.html',1,'']]],
  ['strategy',['Strategy',['../classStrategy.html',1,'']]],
  ['strategymacd',['StrategyMACD',['../classStrategyMACD.html',1,'']]],
  ['strategymixer',['StrategyMixer',['../classStrategyMixer.html',1,'']]]
];
