classdef StrategyMixerMock < Strategy
   properties
       strategies  
       
   end
   events 
      buy %  event for the engine
      sell 
   end
    methods
        function obj = StrategyMixerMock()

        end
        function addstrategy(obj,individual)
            fh =@obj.generateSignal;
            addlistener(individual,'buy',fh);
            addlistener(individual,'sell',fh);
        end
        
        function generateSignal(obj,publisher,evtobj)
            % simple redirection
      
            price = evtobj.price;
            % just pass through 
            if strcmp(evtobj.EventName,'sell')  
                obj.notify('sell',EventBuySell(price,'mixer'));
            elseif strcmp(evtobj.EventName,'buy') 
                obj.notify('buy',EventBuySell(price,'mixer'));
            end            

      
            
        end
    end
end